/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import static entities.ILibrary.LOAN_LIMIT;
import static entities.ILibrary.LOAN_PERIOD;
import static entities.ILibrary.MAX_FINES_OWED;
import entities.ILoan.LoanState;
import entities.helpers.BookHelper;
import entities.helpers.IBookHelper;
import entities.helpers.ILoanHelper;
import entities.helpers.IPatronHelper;
import entities.helpers.LoanHelper;
import entities.helpers.PatronHelper;
import entities.ILoan;
import entities.Patron;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Salim
 */

public class LibraryTest {
    @Mock IBookHelper mockBookHelper;
    @Mock IPatronHelper mockPatronHelper;
    @Mock ILoanHelper mockLoanHelper;
    Map<Integer, IBook> catalog;
    Map<Integer, IPatron> patrons;
    Map<Integer, ILoan> loans;
    Map<Integer, ILoan> currentLoans;
    Map<Integer, IBook> damagedBooks;
    static IBookHelper bookHelper = new BookHelper();
    static IPatronHelper patronHelper = new PatronHelper();
    static ILoanHelper loanHelper = new LoanHelper();
    static java.util.Calendar calendar;
    Library library = new Library(bookHelper, patronHelper, loanHelper);
    //public LibraryTest() {
    //}
    
    @Before
    public void setUpBeforeClass() throws Exception {
    bookHelper = new BookHelper();
    patronHelper = new PatronHelper();
    loanHelper = new LoanHelper();
    }

    @Test
    public void testGetPatronList() {
    }

    @Test
    public void testGetBookList() {
    }

    @Test
    public void testGetCurrentLoansList() {
    }

    @Test
    public void testAddPatron() {
        //arrange
        String lastName = "Salim";
        String firstName = "George";
        String emailAddress = "email@domain.com.au";
        long phoneNo = 94678251;
        int id = 123;
        
        //act
        IPatron patron = library.addPatron(lastName, firstName, emailAddress, phoneNo);
        
        //assert
        assertNotNull(patron);
        List<IPatron> pList = library.getPatronList();
        assertTrue(pList.size() == 1);
    }

    @Test
    public void testAddBook() {
        //arrange
        String author = "Author";
        String title = "Book";
        String callNumber = "7";
        int bookId = 1;
        
        //act
        IBook book = library.addBook(author, title, callNumber);
        
        //assert
        assertNotNull(book);
        List<IBook> bList = library.getBookList();
        assertTrue(bList.size() == 1);
    }

    @Test
    public void testGetPatronById() {
    }

    @Test
    public void testGetBookById() {
    }

    @Test
    public void testPatronWillReachLoanMax() {
    }

    @Test
    public void testPatronCanBorrow() {
    }

    @Test
    public void testIssueLoan() {
        //arrange
        String author = "Author";
        String title = "Book";
        String callNumber = "7";
        int bookId = 1;
        IBook book = library.addBook(author, title, callNumber);
        
        String lastName = "Salim";
        String firstName = "George";
        String emailAddress = "email@domain.com.au";
        long phoneNo = 94678251;
        int id = 123;
        IPatron patron = library.addPatron(lastName, firstName, emailAddress, phoneNo);
        
        //act
        ILoan loan = library.issueLoan(book, patron);
        
        //assert
        assertNotNull(loan);
        List<ILoan> lList = library.getCurrentLoansList();
        assertEquals(true,library.patronCanBorrow(patron));
    }

    @Test
    public void testGetCurrentLoanByBookId() {
    }

    @Test
    public void testCommitLoan() {
        //arrange
        String author = "Author";
        String title = "Book";
        String callNumber = "7";
        int bookId = 1;
        IBook book = library.addBook(author, title, callNumber);
        
        String lastName = "Salim";
        String firstName = "George";
        String emailAddress = "email@domain.com.au";
        long phoneNo = 94678251;
        int id = 123;
        IPatron patron = library.addPatron(lastName, firstName, emailAddress, phoneNo);
        
        ILoan loan = library.issueLoan(book, patron);
        
        //Act
        library.commitLoan(loan);
        
        //assert
        //assertNotNull(loan);
        //List<ILoan> lList = library.getCurrentLoansList();
        assertNotNull(library.getCurrentLoansList());
    }

    @Test
    public void testDischargeLoan() {
    }

    @Test
    public void testCheckCurrentLoansOverDue() {
        
    }

    @Test
    public void testCalculateOverDueFine() {
        //arrange
        String author = "Author";
        String title = "Book";
        String callNumber = "7";
        int bookId = 1;
        IBook book = library.addBook(author, title, callNumber);
        
        String lastName = "Salim";
        String firstName = "George";
        String emailAddress = "email@domain.com.au";
        long phoneNo = 94678251;
        int loanPeriod = 2;
        int id = 10;
        IPatron patron = library.addPatron(lastName, firstName, emailAddress, phoneNo);
        
        //Act
        Date date = Calendar.getInstance().getDate();
        Calendar.getInstance().setDate(date);
        ILoan loan = library.issueLoan(book, patron);
        library.commitLoan(loan);
        Calendar.getInstance().getDueDate(loanPeriod);
        Calendar.getInstance().incrementDate(id);
        library.dischargeLoan(loan, false);
        
        //assert
        double actualFine = library.payFine(patron, MAX_FINES_OWED);
        double expectedFine = library.calculateOverDueFine(loan);
        assertEquals(expectedFine,actualFine,expectedFine);
    }

    @Test
    public void testPayFine() {
    }

    @Test
    public void testRepairBook() {
    }
    
}
