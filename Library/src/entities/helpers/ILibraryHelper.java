package entities.helpers;

import java.io.Serializable;

import entities.ILibrary;

public interface ILibraryHelper extends Serializable {

	ILibrary makeLibrary(IBookHelper bookHelper, IPatronHelper patronHelper, ILoanHelper loanHelper);

	public ILibrary loadLibrary();

	public void saveLibrary(ILibrary library);

}
