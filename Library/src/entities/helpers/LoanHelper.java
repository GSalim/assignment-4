package entities.helpers;

import entities.IBook;
import entities.ILoan;
import entities.IPatron;
import entities.Loan;

public class LoanHelper implements ILoanHelper {

	private static final long serialVersionUID = 1L;

	@Override
	public ILoan makeLoan(IBook book, IPatron patron) {
		return new Loan(book, patron);
	}
}
