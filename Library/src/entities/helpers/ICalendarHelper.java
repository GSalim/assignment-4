package entities.helpers;

import java.io.Serializable;

import entities.ICalendar;

public interface ICalendarHelper extends Serializable {

	public ICalendar loadCalendar();

	public void saveCalendar();

}
