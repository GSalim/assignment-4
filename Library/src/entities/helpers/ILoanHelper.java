package entities.helpers;

import java.io.Serializable;

import entities.IBook;
import entities.ILoan;
import entities.IPatron;

public interface ILoanHelper extends Serializable {

	ILoan makeLoan(IBook book, IPatron patron);

}