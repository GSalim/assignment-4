package entities.helpers;

import java.io.Serializable;

import entities.IPatron;

public interface IPatronHelper extends Serializable {

	public IPatron makePatron(String lastName, String firstName, String email, long phoneNo, int id);

}
